class CreatePosts < ActiveRecord::Migration[5.2]
  def change
    create_table :posts do |t|
      t.integer :author
      t.string :title
      t.string :description
      t.string :content
      t.integer :community_votes
      t.integer :annonymou_votes

      t.timestamps
    end
  end
end
