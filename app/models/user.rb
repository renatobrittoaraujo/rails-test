class User < ApplicationRecord
  has_secure_password

  validates :email, presence: true, uniqueness: true
  validates :user_nick, presence: true, uniqueness: true
  validates :password, presence: true, length: {:within => 8..20}
end
