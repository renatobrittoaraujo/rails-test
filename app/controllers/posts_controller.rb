class PostsController < ApplicationController

    require "net/http"

    def index
        if params.has_key?(:category)
            @posts = Post.where(category_id: params['category'])
        else
            @posts = Post.all
        end
    end

    require 'net/http'
    require 'uri'

    def image_exists?(url)
        url = URI.parse(url_str)
        Net::HTTP.start(url.host, url.port) do |http|
            http.head(url.request_uri).code == '200'
        end
    rescue
        false
    end

    def create
        if current_user.nil?
            redirect_to root_path
        end

        @post = Post.new(post_params)
        @post.author = current_user.id
        @post.date = DateTime.now
        @post.community_votes = 0
        @post.annonymou_votes = 0
        
        if !image_exists?(@post.image)
            @post.image = nil
        end

        if @post.save
            redirect_to @post
        else
            render 'new'
        end
    end

    def new
        if current_user
            @post = Post.new
        else
            flash[:warning] = "You must be logged in to post"
            redirect_to root_path
        end
    end

    def show
        if !Post.exists?(params[:id])
            flash[:warning] = "Post was not found"
            redirect_to root_path
        else        
            @post = Post.find(params[:id])
        end
    end

    def upvote
        @post = Post.find(params[:id])
    end
        

    private

        def post_params
            params.require(:post).permit(:title, :content, :description, :image, :category_id)
        end

end